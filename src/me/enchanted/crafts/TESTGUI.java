package me.enchanted.crafts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class TESTGUI implements Listener {

	public static HashMap<Player, Integer> page = new HashMap<Player, Integer>();

	public static void openTest(Player p, ShapedRecipe r, Integer pg) {
		Inventory inv = null;
		if (r == null) {
			inv = Bukkit.createInventory(null, 54, "�eRecipes");
			
			for(int i = 0; i < inv.getSize(); i++) {
				ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
				ItemMeta im = is.getItemMeta();
				
				im.setDisplayName("�7");
				
				is.setItemMeta(im);
				
				inv.setItem(i, is);
			}
			
			if (pg == 1) {
				page.put(p, pg);
				inv.setItem(10, setInfo(Crafts.dragonSword.getResult()));
				inv.setItem(11, setInfo(Crafts.enlighteningPack.getResult()));
				inv.setItem(12, setInfo(Crafts.flamingArtifact.getResult()));
				inv.setItem(13, setInfo(Crafts.goldenHead.getResult()));
				inv.setItem(14, setInfo(Crafts.goldPack.getResult()));
				inv.setItem(15, setInfo(Crafts.ironPack.getResult()));
				inv.setItem(16, setInfo(Crafts.lightApple.getResult()));
				inv.setItem(19, setInfo(Crafts.nectar.getResult()));
				inv.setItem(20, setInfo(Crafts.philoPick.getResult()));
				inv.setItem(21, setInfo(Crafts.anduril.getResult()));
				inv.setItem(22, setInfo(Crafts.deliciousMeal.getResult()));
				inv.setItem(23, setInfo(Crafts.sugarRush.getResult()));
				inv.setItem(24, setInfo(Crafts.leatherEconomy.getResult()));
				inv.setItem(25, setInfo(Crafts.hermesBoots.getResult()));
				inv.setItem(28, setInfo(Crafts.quickPick.getResult()));
				inv.setItem(29, setInfo(Crafts.dustOfLight.getResult()));
				inv.setItem(30, setInfo(Crafts.tarnHelm.getResult()));
				inv.setItem(31, setInfo(Crafts.fusionArmor.getResult()));
				inv.setItem(32, setInfo(Crafts.lightAnvil.getResult()));
				inv.setItem(33, setInfo(Crafts.lightEnchantmentTable.getResult()));
				inv.setItem(34, setInfo(Crafts.velocity.getResult()));
				inv.setItem(49, close());
				inv.setItem(53, next(p));
			} else if (pg == 2) {
				page.put(p, pg);
				inv.setItem(10, setInfo(Crafts.vorpalSword.getResult()));
				inv.setItem(11, setInfo(Crafts.sharpness.getResult()));
				inv.setItem(12, setInfo(Crafts.power.getResult()));
				inv.setItem(13, setInfo(Crafts.protection.getResult()));
				inv.setItem(14, setInfo(Crafts.projectileProtection.getResult()));
				inv.setItem(15, setInfo(Crafts.thoth.getResult()));
				inv.setItem(16, setInfo(Crafts.tabletsOfDestiny.getResult()));
				inv.setItem(19, setInfo(Crafts.dragonarmor.getResult()));
				inv.setItem(20, setInfo(Crafts.barbarianchestplate.getResult()));
				inv.setItem(21, setInfo(Crafts.axeofperun.getResult()));
				inv.setItem(22, setInfo(Crafts.expertseal.getResult()));
				inv.setItem(23, setInfo(Crafts.exodus.getResult()));
				inv.setItem(23, setInfo(Crafts.arrowEco.getResult()));
				inv.setItem(24, setInfo(Crafts.hideOfLeviathan.getResult()));
				inv.setItem(25, setInfo(Crafts.artemis.getResult()));
				inv.setItem(28, setInfo(Crafts.bloodlust.getResult()));
				inv.setItem(45, back(p));
				inv.setItem(49, close());
			}
		} else {

			inv = Bukkit.createInventory(null, 54, "�eRecipe");
			
			for(int i = 0; i < inv.getSize(); i++) {
				ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
				ItemMeta im = is.getItemMeta();
				
				im.setDisplayName("�7");
				
				is.setItemMeta(im);
				
				inv.setItem(i, is);
			}
			
			int g = 3;
			int y = 0;
			for (String is : r.getShape()) {

				for (int i = 0; i < is.length(); i++) {
					if (y == 3) {
						y = 0;
						g += 6;

					}
					inv.setItem(g, r.getIngredientMap().get(is.charAt(i)));
					y++;
					g++;
				}
				// i.setItem(g, r.getIngredientMap().get());
			}

			inv.setItem(40, r.getResult());
			inv.setItem(49, close());
		}

		p.openInventory(inv);
	}

	private static ItemStack setInfo(ItemStack result) {
		List<String> l = new ArrayList<String>();

		ItemStack is = result;
		ItemMeta im = is.getItemMeta();
		im.setDisplayName("�a" + CraftsInfo.crafts.getName(getMetaUHC(is)));
		if (im.getLore() != null && im.hasLore()) {
			for (String s : im.getLore()) {
				l.add(s);
			}
		}

		l.add("�7");
		l.add("�7Max Crafts: �b" + CraftsInfo.crafts.getAmount(getMetaUHC(result)));

		im.setLore(l);
		is.setItemMeta(im);

		return is;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().getTitle().equals("�eRecipes")) {
			e.setCancelled(true);
			if(e.getCurrentItem() == null) return;
			if (e.getCurrentItem().equals(next(p))) {
				openTest(p, null, page.get(p) + 1);
				return;
			} else if (e.getCurrentItem().equals(back(p))) {
				openTest(p, null, page.get(p) - 1);
				return;
			} else if (e.getCurrentItem().equals(close())) {
				p.closeInventory();
				return;
			}
			if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()
					&& e.getCurrentItem().getItemMeta().hasDisplayName()
					&& e.getCurrentItem().getItemMeta().getDisplayName().equals("�aGolden Head")) {
				openTest(p, Crafts.goldenHead, null);
			}
			for (ShapedRecipe r : CraftsInfo.results) {
				if (e.getCurrentItem().equals(setInfo(r.getResult()))) {
					openTest(p, r, null);
				}
			}
		} else if (e.getInventory().getTitle().equals("�eRecipe")) {

			e.setCancelled(true);

			if(e.getCurrentItem() == null) return;
			if (e.getCurrentItem().equals(close())) {
				openTest(p, null, page.get(p) == null ? 1 : page.get(p));
			}
		}
	}

	public static ItemStack close() {
		ItemStack c = new ItemStack(Material.BARRIER);
		ItemMeta cm = c.getItemMeta();
		cm.setDisplayName("�cClose");
		c.setItemMeta(cm);

		return c;
	}

	public static ItemStack next(Player p) {
		ItemStack n = new ItemStack(Material.ARROW);
		ItemMeta nm = n.getItemMeta();
		nm.setDisplayName("�aNext Page");
		nm.setLore(Arrays.asList("�ePage " + (page.get(p)+1)));
		n.setItemMeta(nm);

		return n;
	}

	public static ItemStack back(Player p) {
		ItemStack b = new ItemStack(Material.ARROW);
		ItemMeta bm = b.getItemMeta();
		bm.setDisplayName("�aPrevious Page");
		bm.setLore(Arrays.asList("�ePage " + (page.get(p)-1)));
		b.setItemMeta(bm);

		return b;
	}

	public static String getMetaUHC(ItemStack i) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return "null";
		}

		return item.getTag().getString("UHC");
	}
}
