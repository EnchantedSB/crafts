package me.enchanted.crafts;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class Crafts {
	
	public static ShapedRecipe lightApple;
	public static ShapedRecipe nectar;
	public static ShapedRecipe dragonSword;
	public static ShapedRecipe enlighteningPack;
	public static ShapedRecipe goldenHead;
	public static ShapedRecipe flamingArtifact;
	public static ShapedRecipe ironPack;
	public static ShapedRecipe goldPack;
	public static ShapedRecipe philoPick;
	public static ShapedRecipe anduril;
	public static ShapedRecipe deliciousMeal;
	public static ShapedRecipe sugarRush;
	public static ShapedRecipe leatherEconomy;
	public static ShapedRecipe hermesBoots;
	public static ShapedRecipe quickPick;
	public static ShapedRecipe dustOfLight;
	public static ShapedRecipe tarnHelm;
	public static ShapedRecipe lightAnvil;
	public static ShapedRecipe lightEnchantmentTable;
	public static ShapedRecipe fusionArmor;
	public static ShapedRecipe velocity;
	public static ShapedRecipe vorpalSword;
	public static ShapedRecipe sharpness;
	public static ShapedRecipe power;
	public static ShapedRecipe protection;
	public static ShapedRecipe projectileProtection;
	public static ShapedRecipe thoth;
	public static ShapedRecipe tabletsOfDestiny;
	public static ShapedRecipe dareDevil;
	public static ShapedRecipe apprenticeSword;
	public static ShapedRecipe apprenticeHelmet;
	public static ShapedRecipe dragonarmor;
	public static ShapedRecipe barbarianchestplate;
	public static ShapedRecipe axeofperun;
	public static ShapedRecipe expertseal;
	public static ShapedRecipe exodus;
	public static ShapedRecipe arrowEco;
	public static ShapedRecipe hideOfLeviathan;
	public static ShapedRecipe artemis;
	public static ShapedRecipe bloodlust;

	public void register() {
		LA();
		NEC();
		DRAGS();
		ENLIGHT();
		GHEAD();
		FLART();
		IRONP();
		GOLDP();
		PHILO();
		ANDURIL();
		DMEAL();
		SRUSH();
		LECON();
		QPICK();
		HERMES();
		DOL();
		TARN();
		FARMOR();
		LANVIL();
		LETABLE();
		VELOCITY();
		VORPAL();
		SHARP();
		POWER();
		PROT();
		PPROT();
		THOTH();
		TOD();
		DARE();
		ASWORD();
		DRAGARMOR();
		BARBCHESTPLATE();
		PERUN();
		EXSEAL();
		EXODUS();
		ARROWECO();
		HIDEOFL();
		ARTEMIS();
		BLOODL();
		
	}
	
	public void LA() {
		ItemStack i = new ItemStack(Material.GOLDEN_APPLE, 1);
		
		lightApple = new ShapedRecipe(setMeta(i, "LightApple"));
		
		lightApple.shape(" G ",
				     "GAG",
				     " G ");
		
		lightApple.setIngredient('G', Material.GOLD_INGOT);
		lightApple.setIngredient('A', Material.APPLE);
		
		Bukkit.addRecipe(lightApple);
	}
	
	public void NEC() {
		ArrayList<String> ar = new ArrayList<String>();
		ar.add("§7Regeneration III (0:10)");
		ar.add("§7Unbreaking I");
		
		ItemStack p = new ItemStack(Material.POTION);
		p = setMeta(p, "Nectar");
		PotionMeta pm = (PotionMeta) p.getItemMeta();
		pm.setLore(ar);
		pm.setDisplayName("§aNectar");
		pm.addEnchant(Enchantment.DURABILITY, 1, true);
		pm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		pm.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * 20, 2),true);
		p.setItemMeta(pm);
		
		nectar = new ShapedRecipe(p);
		
		nectar.shape(" E ",
				         "GMG",
				         " B ");
		
		nectar.setIngredient('G', Material.GOLD_INGOT);
		nectar.setIngredient('E', Material.EMERALD);
		nectar.setIngredient('M', Material.MELON);
		nectar.setIngredient('B', Material.GLASS_BOTTLE);
		
		Bukkit.addRecipe(nectar);
	}
	
	public void DRAGS() {
		dragonSword = new ShapedRecipe(Items.dragonSword);
		
		dragonSword.shape(" B ",
				               " D ",
				               "OBO");
		
		dragonSword.setIngredient('D', Material.DIAMOND_SWORD);
		dragonSword.setIngredient('B', Material.BLAZE_POWDER);
		dragonSword.setIngredient('O', Material.OBSIDIAN);
		
		Bukkit.addRecipe(dragonSword);
	}
	
	public void ENLIGHT() {
		ItemStack i = new ItemStack(Material.EXP_BOTTLE, 8);
		
		enlighteningPack = new ShapedRecipe(setMeta(i, "EnlighteningPack"));
		
		enlighteningPack.shape(" R ",
				               "RBR",
				               " R ");
		
		enlighteningPack.setIngredient('R', Material.REDSTONE_BLOCK);
		enlighteningPack.setIngredient('B', Material.GLASS_BOTTLE);
		
		Bukkit.addRecipe(enlighteningPack);
	}
	
	@SuppressWarnings("deprecation")
	public void GHEAD() {
		
		goldenHead = new ShapedRecipe(Items.goldenHead);
		
		goldenHead.shape("GGG",
				         "GHG",
				         "GGG");
		
		goldenHead.setIngredient('G', Material.GOLD_INGOT);
		goldenHead.setIngredient('H', Material.SKULL_ITEM, 3);
		
		Bukkit.addRecipe(goldenHead);
	}
	
	@SuppressWarnings("deprecation")
	public void FLART() {
		ItemStack i = new ItemStack(Material.BLAZE_ROD, 1);
		
		flamingArtifact = new ShapedRecipe(setMeta(i, "FlamingArtifact"));
		
		flamingArtifact.shape("GLG",
				         "GFG",
				         "GLG");
		
		flamingArtifact.setIngredient('G', Material.STAINED_GLASS, 1);
		flamingArtifact.setIngredient('L', Material.LAVA_BUCKET);
		flamingArtifact.setIngredient('F', Material.FIREWORK);
		
		Bukkit.addRecipe(flamingArtifact);
	}
	
	public void IRONP() {
		ItemStack i = new ItemStack(Material.IRON_INGOT, 10);
		
		ironPack = new ShapedRecipe(setMeta(i, "IronPack"));
		
		ironPack.shape("III",
				         "ICI",
				         "III");
		
		ironPack.setIngredient('I', Material.IRON_ORE);
		ironPack.setIngredient('C', Material.COAL);
		
		Bukkit.addRecipe(ironPack);
	}
	
	public void GOLDP() {
		ItemStack i = new ItemStack(Material.GOLD_INGOT, 10);
		
		goldPack = new ShapedRecipe(setMeta(i, "GoldPack"));
		
		goldPack.shape("GGG",
				         "GCG",
				         "GGG");
		
		goldPack.setIngredient('G', Material.GOLD_ORE);
		goldPack.setIngredient('C', Material.COAL);
		
		Bukkit.addRecipe(goldPack);
	}
	
	public void PHILO() {
		
		philoPick = new ShapedRecipe(Items.philoPick);
		
		philoPick.shape("IGI",
				         "LSL",
				         " S ");
		
		philoPick.setIngredient('I', Material.IRON_ORE);
		philoPick.setIngredient('G', Material.GOLD_ORE);
		philoPick.setIngredient('L', Material.LAPIS_BLOCK);
		philoPick.setIngredient('S', Material.STICK);
		
		Bukkit.addRecipe(philoPick);
	}
	
	public void ANDURIL() {
		
		anduril = new ShapedRecipe(Items.anduril);
		
		anduril.shape("FIF",
					  "FIF",
					  "FBF");
		
		anduril.setIngredient('F',Material.FEATHER);
		anduril.setIngredient('I',Material.IRON_BLOCK);
		anduril.setIngredient('B',Material.BLAZE_ROD);
		
		Bukkit.addRecipe(anduril);
	}
	
	public void DMEAL() {
		ItemStack i = new ItemStack(Material.COOKED_BEEF, 10);
		
		deliciousMeal = new ShapedRecipe(setMeta(i, "DeliciousMeal"));
		
		deliciousMeal.shape("BBB",
							"BCB",
							"BBB");
		
		deliciousMeal.setIngredient('B', Material.RAW_BEEF);
		deliciousMeal.setIngredient('C', Material.COAL);
		
		Bukkit.addRecipe(deliciousMeal);
	}
	
	public void SRUSH() {
		ItemStack i = new ItemStack(Material.SUGAR_CANE, 4);
		
		sugarRush = new ShapedRecipe(setMeta(i, "SugarRush"));
		
		sugarRush.shape("   ",
							" P ",
							"GSG");
		
		sugarRush.setIngredient('S', Material.SUGAR);
		sugarRush.setIngredient('P', Material.SAPLING);
		sugarRush.setIngredient('G', Material.SEEDS);
		
		Bukkit.addRecipe(sugarRush);
	}
	
	public void LECON() {
		ItemStack i = new ItemStack(Material.LEATHER, 8);
		
		leatherEconomy = new ShapedRecipe(setMeta(i, "LeatherEconomy"));
		
		leatherEconomy.shape("SLS",
							 "SLS",
							 "SLS");
		
		leatherEconomy.setIngredient('S', Material.STICK);
		leatherEconomy.setIngredient('L', Material.LEATHER);
		
		Bukkit.addRecipe(leatherEconomy);
	}
	
	public void QPICK() {
		
		quickPick = new ShapedRecipe(Items.quickPick);
		
		quickPick.shape("III",
				          "CSC",
		                  " S ");
		
		quickPick.setIngredient('I', Material.IRON_ORE);
		quickPick.setIngredient('C', Material.COAL);
		quickPick.setIngredient('S', Material.STICK);
		
		Bukkit.addRecipe(quickPick);
	}
	
	@SuppressWarnings("deprecation")
	public void HERMES() {
		
		hermesBoots = new ShapedRecipe(Items.hermes);
		
		hermesBoots.shape("DHD",
				          "PBP",
		                  "F F");
		
		hermesBoots.setIngredient('B', Material.DIAMOND_BOOTS);
		hermesBoots.setIngredient('F', Material.FEATHER);
		hermesBoots.setIngredient('P', Material.BLAZE_POWDER);
		hermesBoots.setIngredient('H', Material.SKULL_ITEM, (byte) 3);
		hermesBoots.setIngredient('D', Material.DIAMOND);
		
		Bukkit.addRecipe(hermesBoots);
	}
	
	public void DOL() {
		ItemStack i = new ItemStack(Material.GLOWSTONE_DUST, 8);
		
		dustOfLight = new ShapedRecipe(setMeta(i, "DustOfLight"));
	
		dustOfLight.shape("RRR",
				          "RFR",
				          "RRR");
		
		dustOfLight.setIngredient('R', Material.REDSTONE);
		dustOfLight.setIngredient('F', Material.FLINT_AND_STEEL);
		
		Bukkit.addRecipe(dustOfLight);
	}
	
	public void TARN() {
		
		tarnHelm = new ShapedRecipe(Items.tarnHelm);
	
		tarnHelm.shape("DID",
				          "DRD",
				          "   ");
		
		tarnHelm.setIngredient('D', Material.DIAMOND);
		tarnHelm.setIngredient('I', Material.IRON_INGOT);
		tarnHelm.setIngredient('R', Material.REDSTONE_BLOCK);
		
		Bukkit.addRecipe(tarnHelm);
	}
	
	public void LANVIL() {
		
		ItemStack is = new ItemStack(Material.ANVIL);
		
		lightAnvil = new ShapedRecipe(setMeta(is, "LightAnvil"));
	
		lightAnvil.shape("III",
				       " B ",
				       "III");
		
		lightAnvil.setIngredient('B', Material.IRON_BLOCK);
		lightAnvil.setIngredient('I', Material.IRON_INGOT);
		
		Bukkit.addRecipe(lightAnvil);
	}
	
	public void LETABLE() {
		
		ItemStack is = new ItemStack(Material.ENCHANTMENT_TABLE);
		
		lightEnchantmentTable = new ShapedRecipe(setMeta(is, "LightEnchantmentTable"));
	
		lightEnchantmentTable.shape(" B ",
				         "ODO",
				         "OEO");
		
		lightEnchantmentTable.setIngredient('B', Material.BOOKSHELF);
		lightEnchantmentTable.setIngredient('E', Material.EXP_BOTTLE);
		lightEnchantmentTable.setIngredient('D', Material.DIAMOND);
		lightEnchantmentTable.setIngredient('O', Material.OBSIDIAN);
		
		Bukkit.addRecipe(lightEnchantmentTable);
	}
	
	public void FARMOR() {
		
		fusionArmor = new ShapedRecipe(Items.fusionArmor);
	
		fusionArmor.shape("HCL",
				       "BB ",
				       "   ");
		
		fusionArmor.setIngredient('B', Material.DIAMOND_BOOTS);
		fusionArmor.setIngredient('L', Material.DIAMOND_LEGGINGS);
		fusionArmor.setIngredient('C', Material.DIAMOND_CHESTPLATE);
		fusionArmor.setIngredient('H', Material.DIAMOND_HELMET);
		
	}
	
	@SuppressWarnings("deprecation")
	public void VELOCITY() {
		
		velocity = new ShapedRecipe(Items.velocity);
	
		velocity.shape(" C ",
				       " S ",
				       " B ");
		
		velocity.setIngredient('C', Material.INK_SACK, 3);
		velocity.setIngredient('S', Material.SUGAR);
		velocity.setIngredient('B', Material.GLASS_BOTTLE);
		
		Bukkit.addRecipe(velocity);
	}
	
	public void VORPAL() {
		
		vorpalSword = new ShapedRecipe(Items.vorpalSword);
	
		vorpalSword.shape(" B ",
				       " S ",
				       " R ");
		
		vorpalSword.setIngredient('R', Material.ROTTEN_FLESH);
		vorpalSword.setIngredient('S', Material.IRON_SWORD);
		vorpalSword.setIngredient('B', Material.BONE);
		
		Bukkit.addRecipe(vorpalSword);
	}
	
	public void SHARP() {
		
		sharpness = new ShapedRecipe(Items.sharpness);
	
		sharpness.shape("F  ",
				        " PP",
				        " PS");
		
		sharpness.setIngredient('F', Material.FLINT);
		sharpness.setIngredient('S', Material.IRON_SWORD);
		sharpness.setIngredient('P', Material.PAPER);
		
		Bukkit.addRecipe(sharpness);
	}
	
	public void POWER() {
		
		power = new ShapedRecipe(Items.power);
	
		power.shape("F  ",
				        " PP",
				        " PB");
		
		power.setIngredient('F', Material.FLINT);
		power.setIngredient('B', Material.BONE);
		power.setIngredient('P', Material.PAPER);
		
		Bukkit.addRecipe(power);
	}
	
	public void PROT() {
		
		protection = new ShapedRecipe(Items.protection);
	
		protection.shape("   ",
				        " PP",
				        " PI");
		
		protection.setIngredient('I', Material.IRON_INGOT);
		protection.setIngredient('P', Material.PAPER);
		
		Bukkit.addRecipe(protection);
	}
	
	public void PPROT() {
		
		projectileProtection = new ShapedRecipe(Items.projProt);
	
		projectileProtection.shape("   ",
				        		   " PP",
				        		   " PA");
		
		projectileProtection.setIngredient('A', Material.ARROW);
		projectileProtection.setIngredient('P', Material.PAPER);
		
		Bukkit.addRecipe(projectileProtection);
	}
	
	public void THOTH() {
		
		thoth = new ShapedRecipe(Items.thoth);
	
		thoth.shape("E  ",
				        " PP",
				        " PF");
		
		thoth.setIngredient('F', Material.FIREBALL);
		thoth.setIngredient('E', Material.EYE_OF_ENDER);
		thoth.setIngredient('P', Material.PAPER);
		
		Bukkit.addRecipe(thoth);
	}
	
	public void TOD() {
		
		tabletsOfDestiny = new ShapedRecipe(Items.tabletsOfDestiny);
	
		tabletsOfDestiny.shape(" M ",
				        "DQB",
				        "EEE");
		
		tabletsOfDestiny.setIngredient('E', Material.EXP_BOTTLE);
		tabletsOfDestiny.setIngredient('D', Material.DIAMOND_SWORD);
		tabletsOfDestiny.setIngredient('Q', Material.BOOK_AND_QUILL);
		tabletsOfDestiny.setIngredient('B', Material.BOW);
		tabletsOfDestiny.setIngredient('M', Material.MAGMA_CREAM);
		
		Bukkit.addRecipe(tabletsOfDestiny);
	}
	
	@SuppressWarnings("deprecation")
	public void DARE() {
		
		dareDevil = new ShapedRecipe(Items.dareDevil);
	
		dareDevil.shape("HS ",
				        "BBB",
				        "B B");
		
		dareDevil.setIngredient('B', Material.BONE);
		dareDevil.setIngredient('H', Material.SKULL_ITEM, (byte)3);
		dareDevil.setIngredient('S', Material.SADDLE);
		
		Bukkit.addRecipe(dareDevil);
	}
	
	@SuppressWarnings("deprecation")
	public void ASWORD() {
		
		apprenticeSword = new ShapedRecipe(Items.apprenticeSword);
	
		apprenticeSword.shape(" R ",
				        " S ",
				        " R ");
		
		apprenticeSword.setIngredient('R', Material.REDSTONE_BLOCK);
		apprenticeSword.setIngredient('S', Material.IRON_SWORD);
		
		Bukkit.addRecipe(apprenticeSword);
	}
	
	@SuppressWarnings("deprecation")
	public void DRAGARMOR() {
		
		dragonarmor = new ShapedRecipe(Items.dragonarmor);
	
		dragonarmor.shape(" M ",
				        " D ",
				        "OAO");
		
		dragonarmor.setIngredient('M', Material.MAGMA_CREAM);
		dragonarmor.setIngredient('D', Material.DIAMOND_CHESTPLATE);
		dragonarmor.setIngredient('O', Material.OBSIDIAN);
		dragonarmor.setIngredient('A', Material.ANVIL);
		
		Bukkit.addRecipe(dragonarmor);
	}
	
	@SuppressWarnings("deprecation")
	public void BARBCHESTPLATE() {
		
		barbarianchestplate = new ShapedRecipe(Items.barbarianchestplate);
		ItemStack stpot = new ItemStack(Material.POTION, 1, (short) 8265);
	
		barbarianchestplate.shape("   ",
				        "BCB",
				        "ISI");
		
		barbarianchestplate.setIngredient('B', Material.BLAZE_ROD);
		barbarianchestplate.setIngredient('C', Material.DIAMOND_CHESTPLATE);
		barbarianchestplate.setIngredient('I', Material.IRON_BLOCK);
		barbarianchestplate.setIngredient('S', Material.POTION, (short)8265);
		Bukkit.addRecipe(barbarianchestplate);

		barbarianchestplate.setIngredient('S', Material.POTION, (short)8201);
		Bukkit.addRecipe(barbarianchestplate);
	}
	
	public void PERUN() {
		
		axeofperun = new ShapedRecipe(Items.axeOfPerun);
	
		axeofperun.shape("DTF",
				        "DS ",
				        " S ");
		
		axeofperun.setIngredient('D', Material.DIAMOND);
		axeofperun.setIngredient('T', Material.TNT);
		axeofperun.setIngredient('F', Material.FIREBALL);
		axeofperun.setIngredient('S', Material.STICK);

		
		Bukkit.addRecipe(axeofperun);
	}
	public void EXSEAL() {
		
		expertseal = new ShapedRecipe(Items.expertSeal);
	
		expertseal.shape("BIB",
				        "GDG",
				        "BIB");
		
		expertseal.setIngredient('B', Material.EXP_BOTTLE);
		expertseal.setIngredient('I', Material.IRON_BLOCK);
		expertseal.setIngredient('G', Material.GOLD_BLOCK);
		expertseal.setIngredient('D', Material.DIAMOND_BLOCK);

		
		Bukkit.addRecipe(expertseal);
	}
	@SuppressWarnings("deprecation")
	public void EXODUS() {
			
		
		exodus = new ShapedRecipe(Items.exodus);
		
		exodus.shape("DDD",
				        "DHD",
				        "ECE");
		
		exodus.setIngredient('D', Material.DIAMOND);
		exodus.setIngredient('H', Material.SKULL_ITEM ,3);
		exodus.setIngredient('C', Material.GOLDEN_CARROT);
		exodus.setIngredient('E', Material.EMERALD);

		
		Bukkit.addRecipe(exodus);
	}
	
	public void ARROWECO() {
		ItemStack i = new ItemStack(Material.ARROW, 20);
		
		arrowEco = new ShapedRecipe(setMeta(i, "ArrowEconomy"));
		
		arrowEco.shape("FFF",
						"SSS",
						"EEE");
		
		arrowEco.setIngredient('F', Material.FLINT);
		arrowEco.setIngredient('S', Material.STICK);
		arrowEco.setIngredient('E', Material.FEATHER);
		
		Bukkit.addRecipe(arrowEco);
	}
	
	public void HIDEOFL() {
		ItemStack i = new ItemStack(Material.ARROW, 20);
		
		hideOfLeviathan = new ShapedRecipe(Items.hideofleviathan);
		
		hideOfLeviathan.shape("LWL", 
								"DdD",
								"P P");
		
		hideOfLeviathan.setIngredient('L', Material.LAPIS_BLOCK);
		hideOfLeviathan.setIngredient('W', Material.WATER_BUCKET);
		hideOfLeviathan.setIngredient('d', Material.DIAMOND_LEGGINGS);
		hideOfLeviathan.setIngredient('D', Material.DIAMOND);
		hideOfLeviathan.setIngredient('P', Material.WATER_LILY);
		
		Bukkit.addRecipe(hideOfLeviathan);
	}
	public void ARTEMIS() {
		
		artemis = new ShapedRecipe(Items.artemis);
		
		artemis.shape("FDF", "FBF", "FEF");
		
		artemis.setIngredient('D', Material.DIAMOND);
		artemis.setIngredient('B', Material.BOW);
		artemis.setIngredient('F', Material.FEATHER);
		artemis.setIngredient('E', Material.EYE_OF_ENDER);

		
		Bukkit.addRecipe(artemis);
	}
	public void BLOODL() {
		
		bloodlust = new ShapedRecipe(Items.bloodLust);
		
		bloodlust.shape("RDR", "RSR", "RBR");
		
		bloodlust.setIngredient('R', Material.REDSTONE_BLOCK);
		bloodlust.setIngredient('B', Material.EXP_BOTTLE);
		bloodlust.setIngredient('S', Material.DIAMOND_SWORD);
		bloodlust.setIngredient('D', Material.DIAMOND);

		
		Bukkit.addRecipe(bloodlust);
	}
	
	public ItemStack setMeta(ItemStack i, String value) {

		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);

		NBTTagCompound compound = new NBTTagCompound();

		compound.setString("UHC", value);

		Itemstack.setTag(compound);

		i = CraftItemStack.asBukkitCopy(Itemstack);

		return i;
	}
	
}