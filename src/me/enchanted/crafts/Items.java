package me.enchanted.crafts;

import java.util.Arrays;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;

import me.enchanted.crafts.utils.Reflections;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagDouble;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;

public class Items {

	private static final Base64 base64 = new Base64();
	public static ItemStack dragonSword = new ItemStack(Material.DIAMOND_SWORD);
	public static ItemStack godSword = new ItemStack(Material.DIAMOND_SWORD);
	public static ItemStack goldenHead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
	public static ItemStack diamondHead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
	public static ItemStack philoPick = new ItemStack(Material.DIAMOND_PICKAXE, 1, (byte) 3);
	public static ItemStack anduril = new ItemStack(Material.IRON_SWORD);
	public static ItemStack vorpalSword = new ItemStack(Material.IRON_SWORD);
	public static ItemStack quickPick = new ItemStack(Material.IRON_PICKAXE);
	public static ItemStack hermes = new ItemStack(Material.DIAMOND_BOOTS);
	public static ItemStack tarnHelm = new ItemStack(Material.DIAMOND_HELMET);
	public static ItemStack fusionArmor = new ItemStack(Material.DIAMOND_CHESTPLATE);
	public static ItemStack velocity = new ItemStack(Material.POTION);
	public static ItemStack sharpness = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack protection = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack projProt = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack thoth = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack tabletsOfDestiny = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack apprenticeHelmet = new ItemStack(Material.IRON_HELMET);
	public static ItemStack apprenticeSword = new ItemStack(Material.IRON_SWORD);
	public static ItemStack dareDevil = new ItemStack(Material.MONSTER_EGG, 1, (short) 100);
	public static ItemStack dragonarmor = new ItemStack(Material.DIAMOND_CHESTPLATE);
	public static ItemStack barbarianchestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
	public static ItemStack axeOfPerun = new ItemStack(Material.DIAMOND_AXE);
	public static ItemStack expertSeal = new ItemStack(Material.NETHER_STAR);
	public static ItemStack exodus = new ItemStack(Material.DIAMOND_HELMET);
	public static ItemStack hideofleviathan = new ItemStack(Material.DIAMOND_LEGGINGS);
	public static ItemStack artemis = new ItemStack(Material.BOW);
	public static ItemStack bloodLust = new ItemStack(Material.DIAMOND_SWORD);

	public void setup() {
		setDragonSword();
		setGoldenHead();
		setPhilo();
		setAnduril();
		setQuickPick();
		setHermes();
		setTarn();
		setFusionArmor();
		setVelocity();
		setVorpalSword();
		setSharp();
		setPower();
		setProt();
		setProjProt();
		setThoth();
		setTOD();
		setApprenticeHelmet();
		setApprenticeSword();
		setDareDevil();
		setDragonArmor();
		setBarbarianChestplate();
		setAxeOfPerun();
		setexpertSeal();
		setExodus();
		setHide();
		setArtemis();
		setBloodlust();
	}

	private void setDragonSword() {
		dragonSword = setMeta(dragonSword, "DragonSword");
		ItemMeta im = dragonSword.getItemMeta();

		im.setDisplayName("§aDragon Sword");
		im.setLore(Arrays.asList("§7Weaponsmith Ultimate"));
		dragonSword.setItemMeta(im);

		dragonSword = changeItemDamage(dragonSword, 8);
	}

	private void setGoldenHead() {

		goldenHead = setMeta(goldenHead, "GoldenHead");
		SkullMeta im = (SkullMeta) goldenHead.getItemMeta();

		im.setOwner("0000000000000P");
		im.setDisplayName("§6Golden Head");

		goldenHead.setItemMeta(im);
	}

	private void setPhilo() {
		philoPick = setMeta(philoPick, "Philo");

		ItemMeta im = philoPick.getItemMeta();

		philoPick.setDurability((short) 1559);

		im.setDisplayName("§aPhilosopher's Pickaxe");
		im.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 2, true);

		philoPick.setItemMeta(im);
	}

	private void setAnduril() {
		anduril = setMeta(anduril, "Anduril");

		ItemMeta im = anduril.getItemMeta();

		im.setDisplayName("§aAndúril");
		im.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		im.setLore(Arrays.asList("§9Resistance I (While Holding)", "§9Speed I (While Holding)", "§9Justice"));

		anduril.setItemMeta(im);
	}

	private void setQuickPick() {
		quickPick = setMeta(quickPick, "QuickPick");

		ItemMeta im = quickPick.getItemMeta();

		im.setDisplayName("§aQuick Pick");
		im.addEnchant(Enchantment.DIG_SPEED, 1, true);

		quickPick.setItemMeta(im);
	}

	private void setHermes() {
		hermes = setMeta(hermes, "HermesBoots");

		ItemMeta im = hermes.getItemMeta();

		im.setDisplayName("§aHermes\' Boots");
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		im.addEnchant(Enchantment.PROTECTION_FALL, 1, true);
		im.addEnchant(Enchantment.DURABILITY, 2, true);
		im.setLore(Arrays.asList("§7", "§7Provides the wearer with", "§710% increase walk speed!"));

		hermes.setItemMeta(im);
	}

	private void setTarn() {
		tarnHelm = setMeta(tarnHelm, "TarnHelm");

		ItemMeta im = tarnHelm.getItemMeta();

		im.setDisplayName("§aTarnhelm");
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
		im.addEnchant(Enchantment.WATER_WORKER, 3, true);

		tarnHelm.setItemMeta(im);
	}

	private void setVelocity() {
		velocity = setMeta(velocity, "Velocity");

		Potion p = new Potion(0);
		PotionMeta im = (PotionMeta) velocity.getItemMeta();
		p.splash();
		im.setDisplayName("§aPotion of Velocity");
		im.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 12, 2), true);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		velocity.setItemMeta(im);

		p.apply(velocity);
	}

	private void setFusionArmor() {
		fusionArmor = setMeta(fusionArmor, "FusionArmor");

		ItemMeta im = fusionArmor.getItemMeta();
		im.setDisplayName("§aFusion Armor");
		im.setLore(Arrays.asList("§7Requires 5 random pieces of", "§7Diamond Armor to craft, and",
				"§7gives a random piece of", "§7Fusion Armor! Craft at your", "§7own risk!"));
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		fusionArmor.setItemMeta(im);
	}

	private void setVorpalSword() {
		vorpalSword = setMeta(vorpalSword, "VorpalSword");

		ItemMeta im = vorpalSword.getItemMeta();

		im.setDisplayName("§aVorpal Sword");
		im.addEnchant(Enchantment.DAMAGE_UNDEAD, 2, true);
		im.addEnchant(Enchantment.LOOT_BONUS_MOBS, 1, true);
		im.addEnchant(Enchantment.DAMAGE_ARTHROPODS, 2, true);

		vorpalSword.setItemMeta(im);
	}

	private void setSharp() {
		sharpness = setMeta(sharpness, "Sharpness");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) sharpness.getItemMeta();

		meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
		sharpness.setItemMeta(meta);
	}

	private void setPower() {
		power = setMeta(power, "Power");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) power.getItemMeta();

		meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
		power.setItemMeta(meta);
	}

	private void setProt() {
		protection = setMeta(protection, "Protection");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) protection.getItemMeta();

		meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		protection.setItemMeta(meta);
	}

	private void setProjProt() {
		projProt = setMeta(projProt, "ProjProt");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) projProt.getItemMeta();

		meta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
		projProt.setItemMeta(meta);
	}

	private void setThoth() {
		thoth = setMeta(thoth, "Thoth");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) thoth.getItemMeta();

		meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
		meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 2, true);
		meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 2, true);
		meta.addStoredEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
		meta.addStoredEnchant(Enchantment.FIRE_ASPECT, 1, true);
		thoth.setItemMeta(meta);
	}

	private void setTOD() {
		tabletsOfDestiny = setMeta(tabletsOfDestiny, "TabletsOfDestiny");
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) tabletsOfDestiny.getItemMeta();

		meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 3, true);
		meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 3, true);
		meta.addStoredEnchant(Enchantment.FIRE_ASPECT, 1, true);
		tabletsOfDestiny.setItemMeta(meta);
	}

	private void setApprenticeHelmet() {
		apprenticeHelmet = setMeta(apprenticeHelmet, "ApprenticeHelmet");
		ItemMeta im = apprenticeHelmet.getItemMeta();

		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
		im.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1, true);
		im.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
		apprenticeHelmet.setItemMeta(im);
	}

	private void setApprenticeSword() {
		apprenticeSword = setMeta(apprenticeSword, "ApprenticeSword");
		
		ItemMeta im = apprenticeSword.getItemMeta();
		im.setDisplayName("§aApprentice Sword");
		im.setLore(Arrays.asList("§b➟ Gains Sharpness I after Grace Period",
				"§b➟ Gains Sharpness II 5 minutes after Grace Period",
				"§b➟ Gains Sharpness III 15 minutes after Grace Period", "§b➟ Gains Sharpness IV during Deathmatch"));

		apprenticeSword.setItemMeta(im);
	}

	private void setDareDevil() {
		dareDevil = setMeta(dareDevil, "Dare");
		ItemMeta im = dareDevil.getItemMeta();
		im.setDisplayName("§cDare Devil");
		im.setLore(Arrays.asList("§aType: §7Skeleton Horse", "§aHealth: 25 Hearts", "§aSpeed: §7Max",
				"§aJump: §73 Blocks", "§eComes with free saddle."));
		dareDevil.setItemMeta(im);
	}

	private void setDragonArmor() {
		dragonarmor = setMeta(dragonarmor, "DragonArmor");
		ItemMeta im = dragonarmor.getItemMeta();
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		im.setDisplayName("§aDragon Armor");
		im.setLore(Arrays.asList("§7Armorsmith Ultimate"));
		dragonarmor.setItemMeta(im);
	}

	private void setBarbarianChestplate() {
		barbarianchestplate = setMeta(barbarianchestplate, "BarbarianChestplate");
		ItemMeta im = barbarianchestplate.getItemMeta();
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		im.setDisplayName("§aBarbarian Chestplate");
		im.setLore(Arrays.asList("§9Strength I (While Wearing)", "§9Resistance I (While Wearing)"));
		barbarianchestplate.setItemMeta(im);
	}

	private void setAxeOfPerun() {
		axeOfPerun = setMeta(axeOfPerun, "AxeOfPerun");
		ItemMeta im = axeOfPerun.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		im.setDisplayName("§6Axe Of Perun");
		im.setLore(Arrays.asList("§9Thunder", "", "§7Strikes Lightning, dealing 1.5 hearts! (8 second cooldown)"));
		axeOfPerun.setItemMeta(im);
	}

	private void setexpertSeal() {
		expertSeal = setMeta(expertSeal, "ExpertSeal");
		ItemMeta im = expertSeal.getItemMeta();
		im.setDisplayName("§aExpert Seal");
		im.setLore(Arrays.asList("§9Favor", "§7Upon right clicking with this legendary item,",
				"§7every item in your inventory with enchantments ",
				"§7will have all it's enchantments raised by 1 level."));
		expertSeal.setItemMeta(im);
	}

	private void setExodus() {
		exodus = setMeta(exodus, "ExodusItem");
		ItemMeta im = exodus.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 3, true);
		im.setDisplayName("§6Exodus");
		im.setLore(Arrays.asList("§9Aeon", "", "§7Regenerates a small portion", "§7of health when hit!", ""));
		exodus.setItemMeta(im);
	}

	private void setHide() {
		hideofleviathan = setMeta(hideofleviathan, "hideItem");
		ItemMeta im = hideofleviathan.getItemMeta();
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		im.addEnchant(Enchantment.WATER_WORKER, 3, true);
		im.addEnchant(Enchantment.OXYGEN, 1, true);
		im.setDisplayName("§aHide Of Leviathan");
		im.setLore(Arrays.asList("§9Maelstrom"));
		hideofleviathan.setItemMeta(im);
	}

	private void setArtemis() {
		artemis = setMeta(artemis, "Artemis");
		ItemMeta im = artemis.getItemMeta();
		im.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
		im.setDisplayName("§cArtemis' Bow");
		im.setLore(Arrays.asList("§9Hunting", "", "§7Has a chance for your arrow", "§7to home to your opponent!"));
		artemis.setItemMeta(im);
	}
	
	private void setBloodlust() {
		bloodLust = setMeta(bloodLust, "bloodlust");
		
		ItemMeta im = bloodLust.getItemMeta();
		im.setDisplayName("§cBloodlust");
		im.setLore(Arrays.asList("§b➟ Gains Sharpness II after 1 kill",
				"§b➟ Gains Sharpness III after 3 kills",
				"§b➟ Gains Sharpness IV after 6 kills", "§b➟ Gains Sharpness V after 10 kills"));
		
		im.addEnchant(Enchantment.DAMAGE_ALL, 1, true);

		bloodLust.setItemMeta(im);
	}

	// Info

	public ItemStack addBookEnchantment(ItemStack item, Enchantment enchantment, int level) {
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
		meta.addStoredEnchant(enchantment, level, true);
		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack fusionHelmet() {
		final ItemStack fusionhelmet = new ItemStack(Material.DIAMOND_HELMET);
		final ItemMeta fusionhelmetmeta = fusionhelmet.getItemMeta();
		fusionhelmetmeta.setDisplayName("§cFusion Helmet");
		fusionhelmetmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionhelmet.setItemMeta(fusionhelmetmeta);
		return fusionhelmet;
	}

	public static ItemStack fusionChestplate() {
		final ItemStack fusionchestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
		final ItemMeta fusionchestplatemeta = fusionchestplate.getItemMeta();
		fusionchestplatemeta.setDisplayName("§cFusion Chestplate");
		fusionchestplatemeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionchestplate.setItemMeta(fusionchestplatemeta);
		return fusionchestplate;
	}

	public static ItemStack fusionLeggings() {
		final ItemStack fusionleggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		final ItemMeta fusionleggingsmeta = fusionleggings.getItemMeta();
		fusionleggingsmeta.setDisplayName("§cFusion Leggings");
		fusionleggingsmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionleggings.setItemMeta(fusionleggingsmeta);
		return fusionleggings;
	}

	public static ItemStack fusionBoots() {
		final ItemStack fusionboots = new ItemStack(Material.DIAMOND_BOOTS);
		final ItemMeta fusionbootsmeta = fusionboots.getItemMeta();
		fusionbootsmeta.setDisplayName("§cFusion Boots");
		fusionbootsmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionboots.setItemMeta(fusionbootsmeta);
		return fusionboots;
	}

	public ItemStack getCustomSkull(String url, String meta) {
		GameProfile profile = new GameProfile(UUID.fromString("82a8c2b1-1b71-4646-a118-45d3a5c7dc20"), null);
		PropertyMap propertyMap = profile.getProperties();
		if (propertyMap == null) {
			throw new IllegalStateException("Profile doesn't contain a property map");
		}
		byte[] encodedData = base64.encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
		propertyMap.put("textures", new Property("textures", new String(encodedData)));
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		if (!meta.equalsIgnoreCase("null")) {
			head = setMeta(head, "DiamondHead");
		}
		ItemMeta headMeta = head.getItemMeta();
		Class<?> headMetaClass = headMeta.getClass();
		Reflections.getField(headMetaClass, "profile", GameProfile.class).set(headMeta, profile);
		head.setItemMeta(headMeta);

		return head;
	}

	public ItemStack setMeta(ItemStack i, String value) {

		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);

		NBTTagCompound compound = new NBTTagCompound();

		compound.setString("UHC", value);

		Itemstack.setTag(compound);

		i = CraftItemStack.asBukkitCopy(Itemstack);

		return i;
	}

	public ItemStack changeItemDamage(ItemStack i, double damage) {
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
		NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();

		NBTTagList modifiers = new NBTTagList();

		// -- attack damage
		NBTTagCompound attrib = new NBTTagCompound();
		attrib.set("AttributeName", new NBTTagString("generic.attackDamage"));
		attrib.set("Name", new NBTTagString("generic.attackDamage"));
		attrib.set("Amount", new NBTTagDouble(damage));
		attrib.set("Operation", new NBTTagInt(0));
		attrib.set("UUIDLeast", new NBTTagInt(894654));
		attrib.set("UUIDMost", new NBTTagInt(2872));
		attrib.set("Slot", new NBTTagString("mainhand"));

		modifiers.add(attrib);
		compound.set("AttributeModifiers", modifiers);

		nmsStack.setTag(compound);
		return CraftItemStack.asBukkitCopy(nmsStack);
	}
}
