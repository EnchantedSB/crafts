package me.enchanted.crafts;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class CraftsInfo {
	
	public static Triple<String, String, Integer> crafts = new Triple<String, String, Integer>();
	public static ArrayList<ShapedRecipe> results = new ArrayList<ShapedRecipe>();
	public static HashMap<ItemStack, ShapedRecipe> shapes = new HashMap<ItemStack, ShapedRecipe>();
	private static ArrayList<ItemStack> aA = new ArrayList<ItemStack>();
	
	public void setup() {
		crafts.put("GoldenHead", "Golden Head", 3);
		crafts.put("IronPack", "Iron Pack", 4);
		crafts.put("GoldPack", "Gold Pack", 4);
		crafts.put("FlamingArtifact", "Flaming Artifact", 2);
		crafts.put("EnlighteningPack", "Enlightening Pack", 3);
		crafts.put("Nectar", "Nectar", 3);
		crafts.put("LightApple", "Light Apple", 4);
		crafts.put("DragonSword", "Dragon Sword", 2);
		crafts.put("Anduril", "And�ril", 2);
		crafts.put("Philo", "Philosopher's Pickaxe", 2);
		crafts.put("DeliciousMeal", "Delicious Meal", 3);
		crafts.put("SugarRush", "Sugar Rush", 3);
		crafts.put("LeatherEconomy", "Leather Economy", 3);
		crafts.put("HermesBoots", "Hermes' Boots", 2);
		crafts.put("QuickPick", "Quick Pick", 3);
		crafts.put("DustOfLight", "Dust Of Light", 2);
		crafts.put("TarnHelm", "Tarnhelm", 3);
		crafts.put("FusionArmor", "Fusion Armor", 2);
		crafts.put("LightAnvil", "Light Anvil", 3);
		crafts.put("LightEnchantmentTable", "Light Enchantment Table", 3);
		crafts.put("Velocity", "Velocity", 3);
		crafts.put("VorpalSword", "Vorpal Sword", 3);
		crafts.put("Sharpness", "Book Of Sharpening", 4);
		crafts.put("Power", "Book Of Power", 4);
		crafts.put("Protection", "Book Of Protection", 4);
		crafts.put("ProjProt", "Artemis' Book", 4);
		crafts.put("Thoth", "Book Of Thoth", 2);
		crafts.put("TabletsOfDestiny", "�6Tablets of Destiny", 1);
		crafts.put("DragonArmor", "Dragon Armor", 2);
		crafts.put("BarbarianChestplate", "Barbarian Chestplate", 1);
		crafts.put("AxeOfPerun", "Axe Of Perun", 1);
		crafts.put("ExpertSeal", "Expert Seal", 1);
		crafts.put("ExodusItem", "Exodus", 1);
		crafts.put("ArrowEconomy", "Arrow Economy", 4);
		crafts.put("hideItem", "Hide Of Leviathan", 1);
		crafts.put("Artemis", "Artemis' Bow", 1);
		crafts.put("bloodlust", "Bloodlust", 1);
		results();
		saA();
	}
	
	void results() {
		results.add(Crafts.dragonSword);
		results.add(Crafts.enlighteningPack);
		results.add(Crafts.flamingArtifact);
		results.add(Crafts.goldenHead);
		results.add(Crafts.goldPack);
		results.add(Crafts.ironPack);
		results.add(Crafts.lightApple);
		results.add(Crafts.nectar);
		results.add(Crafts.philoPick);
		results.add(Crafts.anduril);
		results.add(Crafts.deliciousMeal);
		results.add(Crafts.sugarRush);
		results.add(Crafts.leatherEconomy);
		results.add(Crafts.hermesBoots);
		results.add(Crafts.quickPick);
		results.add(Crafts.dustOfLight);
		results.add(Crafts.tarnHelm);
		results.add(Crafts.lightAnvil);
		results.add(Crafts.lightEnchantmentTable);
		results.add(Crafts.fusionArmor);
		results.add(Crafts.velocity);
		results.add(Crafts.vorpalSword);
		results.add(Crafts.sharpness);
		results.add(Crafts.protection);
		results.add(Crafts.projectileProtection);
		results.add(Crafts.power);
		results.add(Crafts.thoth);
		results.add(Crafts.tabletsOfDestiny);
		results.add(Crafts.dragonarmor);
		results.add(Crafts.barbarianchestplate);
		results.add(Crafts.axeofperun);
		results.add(Crafts.expertseal);
		results.add(Crafts.exodus);
		results.add(Crafts.arrowEco);
		results.add(Crafts.hideOfLeviathan);
		results.add(Crafts.artemis);
		results.add(Crafts.bloodlust);

		
		
		for(ShapedRecipe sr : results) {
			shapes.put(sr.getResult(), sr);
		}
	}
	
	void saA() {
		aA.add(Items.anduril);
		aA.add(Items.philoPick);
		aA.add(Items.quickPick);
		aA.add(Items.hermes);
		aA.add(Items.tarnHelm);
		aA.add(Items.fusionArmor);
		aA.add(Items.bloodLust);
	}
	
	public static ArrayList<ItemStack> aA(){
		return aA;
	}
}
