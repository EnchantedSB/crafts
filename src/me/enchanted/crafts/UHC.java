package me.enchanted.crafts;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import me.enchanted.crafts.commands.RecipesCommand;
import me.enchanted.crafts.listeners.ItemsListener;
import me.enchanted.crafts.listeners.CraftListener;
import me.enchanted.crafts.listeners.PlayerListener;
import me.enchanted.uhccore.UHCAPI;
import me.enchanted.uhccore.UHCCore;
import me.enchanted.uhccore.utils.GameManager;
import me.enchanted.uhccore.utils.RandomUtils;

public class UHC extends JavaPlugin {

	private static UHC instance;
	private Crafts c = new Crafts();
	private Items items = new Items();
	private CraftsInfo cI = new CraftsInfo();
	private UHCAPI api = new UHCAPI();
	private boolean debug = true;

	public HashMap<UUID, Integer> cooldownperun = new HashMap<UUID, Integer>();
	public int peruncooldowntime = 4;

	public void onEnable() {
		instance = this;
		getCommand("recipes").setExecutor(new RecipesCommand());
		getServer().getPluginManager().registerEvents(new CraftListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new TESTGUI(), this);
		getServer().getPluginManager().registerEvents(new ItemsListener(), this);
		items.setup();
		c.register();
		cI.setup();
				

		Anduril();
		Perun();

		if (debug == true) {

			new BukkitRunnable() {

				@Override
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						for (ItemStack is : p.getInventory().getContents()) {
							if(is !=null) {
							if (is.hasItemMeta() && is.getItemMeta().hasDisplayName()
									&& is.getItemMeta().getDisplayName().equalsIgnoreCase("§aApprentice Sword")) {
								is.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, api.getApprentice());
							}
							}
						}
					}

				}
			}.runTaskTimer(this, 0, 10);
		}
	}
	

	public void Perun() {
		new BukkitRunnable() {

			@Override
			public void run() {

				if (cooldownperun.isEmpty()) {
					return;
				}
				for (UUID uuid : cooldownperun.keySet()) {
					int timeleftperun = cooldownperun.get(uuid);
					if (timeleftperun <= 0) {
						cooldownperun.remove(uuid);
					} else {
						cooldownperun.put(uuid, timeleftperun - 1);
					}
				}

			}

		}.runTaskTimer(this, 0, 20);
	}

	public void Anduril() {
		new BukkitRunnable() {

			@Override
			public void run() {

				for (Player p : Bukkit.getOnlinePlayers()) {
					if (p.getInventory().getBoots() != null && p.getInventory().getBoots().equals(Items.hermes)) {
						p.setWalkSpeed(0.23f);
					} else {
						p.setWalkSpeed(0.2f);
					}
					if (p.getItemInHand().equals(Items.anduril)) {
						p.removePotionEffect(PotionEffectType.SPEED);
						p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 45, 0));
						p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 45, 0));
					}
					if (p.getInventory().getChestplate() != null) {
						if (p.getInventory().getChestplate().equals(Items.barbarianchestplate)) {
							p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 45, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 45, 0));
						}
					}
				}

			}
		}.runTaskTimer(this, 0, 5);
	}

	public static UHC getInstance() {
		return instance;
	}
}
