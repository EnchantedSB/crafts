package me.enchanted.crafts.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

import me.enchanted.crafts.Crafts;
import me.enchanted.crafts.CraftsInfo;
import me.enchanted.crafts.Items;
import me.enchanted.crafts.UHC;

public class CraftListener implements Listener {

	public static HashMap<UUID, HashMap<String, Integer>> crafts = new HashMap<UUID, HashMap<String, Integer>>();

	@SuppressWarnings({ "unused", "deprecation" })
	@EventHandler
	public static void onInventoryClick(InventoryClickEvent e) {
		// check whether the event has been cancelled by another plugin
		if (!e.isCancelled()) {
			HumanEntity ent = e.getWhoClicked();

			if (ent instanceof Player) {
				Player p = (Player) ent;
				Inventory inv = e.getInventory();

				if (inv instanceof AnvilInventory) {
					AnvilInventory anvil = (AnvilInventory) inv;
					InventoryView view = e.getView();
					int rawSlot = e.getRawSlot();

					if (rawSlot == view.convertSlot(rawSlot)) {
						System.out.print(rawSlot);
						if (CraftsInfo.aA().contains(e.getCursor())) {
							e.setCancelled(true);
							if (p.getInventory().firstEmpty() == -1) {
								p.getWorld().dropItemNaturally(p.getLocation(), e.getCursor());
							} else {
								p.getInventory().addItem(e.getCursor());
							}
							e.setCursor(new ItemStack(Material.AIR));
							e.setCurrentItem(new ItemStack(Material.AIR));
							p.closeInventory();
							p.sendMessage("�cYou cannot put this item in an anvil!");
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onTest(InventoryClickEvent e) {
		if (e.getInventory() instanceof CraftingInventory) {
			if (e.getSlot() == 0) {
				if (((CraftingInventory) e.getInventory()).getRecipe() != null
						&& ((CraftingInventory) e.getInventory()).getRecipe().equals(Crafts.fusionArmor)) {
					return;
				}
				if (e.getCurrentItem().equals(Items.fusionArmor)) {
					e.getWhoClicked().sendMessage("H");
					onCraft(new CraftItemEvent(Crafts.fusionArmor, e.getView(), SlotType.RESULT, 0, ClickType.LEFT,
							InventoryAction.PICKUP_ALL));

				}
			}
			new BukkitRunnable() {

				@Override
				public void run() {

					onL(new PrepareItemCraftEvent((CraftingInventory) e.getInventory(), e.getView(), false));
					t((Player) e.getWhoClicked(), (CraftingInventory) e.getInventory());

				}
			}.runTaskLater(UHC.getInstance(), 2);
		}
	}

	@SuppressWarnings({ "serial" })
	@EventHandler
	public void onL(final PrepareItemCraftEvent e) {
		final Player p = (Player) e.getView().getPlayer();
		try {
			if (crafts.get(p.getUniqueId()) == null) {
				crafts.put(p.getUniqueId(), new HashMap<String, Integer>() {
					{
						for (String s : Lists.newArrayList(CraftsInfo.crafts.getKeysIterator())) {
							put(s, 0);
						}

					}
				});
			}

			if (e.getInventory().getResult() == null || getMetaUHC(e.getInventory().getResult()) == null) {
				return;
			}

			int amount = CraftsInfo.crafts.getAmount(getMetaUHC(e.getInventory().getResult()));

			if (crafts.get(p.getUniqueId()).get(getMetaUHC(e.getInventory().getResult())) >= amount) {
				e.getInventory().setResult(null);
				p.updateInventory();

				return;
			}

		} catch (Exception ex) {
			return;
		}
	}

	private void t(Player p, CraftingInventory inv) {
		try {
			@SuppressWarnings("serial")
			ArrayList<Material> dmnds = new ArrayList<Material>() {
				{
					add(Material.DIAMOND_CHESTPLATE);
					add(Material.DIAMOND_BOOTS);
					add(Material.DIAMOND_HELMET);
					add(Material.DIAMOND_LEGGINGS);
				}
			};
			boolean chestplate = false;
			int diamondPieces = 0;
			for (ItemStack ms : inv.getMatrix()) {
				if (ms.getType().equals(Material.DIAMOND_CHESTPLATE)) {
					chestplate = true;
				}
				if (dmnds.contains(ms.getType())) {
					diamondPieces++;
				}
				if (diamondPieces == 5 && chestplate == true) {
					/*
					 * ItemStack test = new ItemStack(Material.DIAMOND_CHESTPLATE); ItemMeta testm =
					 * test.getItemMeta();
					 * 
					 * testm.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
					 * test.setItemMeta(testm);
					 */
					new BukkitRunnable() {

						@Override
						public void run() {
							int amount = CraftsInfo.crafts.getAmount("FusionArmor");

							if (crafts.get(p.getUniqueId()).get("FusionArmor") >= amount) {
								inv.setItem(0, new ItemStack(Material.AIR));
								p.updateInventory();

								return;
							} else {
								inv.setItem(0, Items.fusionArmor);
								p.updateInventory();
							}

						}
					}.runTaskLater(UHC.getInstance(), 2);
				}
			}
		} catch (Exception e) {
			return;
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	private void onCraft(CraftItemEvent e) {
		try {
			Player p = (Player) e.getWhoClicked();
			if (e.getCursor().equals(e.getInventory().getResult())) {
				if (p.getInventory().firstEmpty() == -1) {
					p.getWorld().dropItemNaturally(p.getLocation(), e.getCursor());
				} else {
					p.getInventory().addItem(e.getCursor());
				}
				e.setCursor(new ItemStack(Material.AIR));
			}
			if (e.getCursor().getType().equals(Material.AIR)) {
				if (e.getInventory().getResult() == null && e.getInventory().getResult().getType().equals(Material.AIR))
					return;
				if (e.isShiftClick() || e.getClick().equals(ClickType.NUMBER_KEY)) {
					e.setCancelled(true);
					p.sendMessage("�cYou cannot shift click to craft as it causes bugs.");
				} else {

					String meta = getMetaUHC(e.getInventory().getResult());
					int amount = CraftsInfo.crafts.getAmount(getMetaUHC(e.getInventory().getResult()));
					if (crafts.get(p.getUniqueId()).get(meta) + 1 > amount) {
						e.setCancelled(true);
						return;
					}
					if (e.getRecipe().equals(Crafts.fusionArmor)) {
						List<ItemStack> fPieces = new ArrayList<ItemStack>();
						fPieces.add(Items.fusionHelmet());
						fPieces.add(Items.fusionChestplate());
						fPieces.add(Items.fusionLeggings());
						fPieces.add(Items.fusionBoots());
						Random r = new Random();
						int fPiece = r.nextInt(fPieces.size());
						ItemStack is = fPieces.get(fPiece);
						System.out.print(is.getType());
						e.setCursor(is);

						((CraftingInventory) e.getInventory()).clear();
						((Player) e.getWhoClicked()).updateInventory();
					}
					crafts.get(p.getUniqueId()).put(meta, crafts.get(p.getUniqueId()).get(meta) == null ? 1
							: crafts.get(p.getUniqueId()).get(meta) + 1);

					p.sendMessage("�eYou have crafted the �a" + CraftsInfo.crafts.getName(meta) + "�e! (�a"
							+ crafts.get(p.getUniqueId()).get(meta) + "�e/" + CraftsInfo.crafts.getAmount(meta) + ")");

				}

			}
		} catch (Exception ex) {
			return;
		}

	}

	@EventHandler
	public void onGHead(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getItem() == null || !e.getItem().hasItemMeta() || !e.getItem().getItemMeta().hasDisplayName())
			return;

		if (getMetaUHC(e.getItem()).equalsIgnoreCase("GoldenHead")) {
			p.removePotionEffect(PotionEffectType.ABSORPTION);
			p.removePotionEffect(PotionEffectType.SPEED);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2 * 20 * 60, 0));
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 8 * 20, 1));
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 20, 1));
			p.sendMessage("�aYou have consumed a Golden Head!");
			if (e.getItem().getAmount() == 1) {
				e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			} else {
				e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
			}
		} else if (getMetaUHC(e.getItem()).equalsIgnoreCase("Head")) {
			p.removePotionEffect(PotionEffectType.SPEED);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 4 * 20, 0));
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5 * 20, 1));
			p.sendMessage("�aYou have consumed a Head!");
			if (e.getItem().getAmount() == 1) {
				e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			} else {
				e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
			}
		}
	}

	@EventHandler
	public void gheadPlace(BlockPlaceEvent e) {
		e.setCancelled(true);
		if (getMetaUHC(e.getItemInHand()).equalsIgnoreCase("GoldenHead")) {
			e.setCancelled(true);
		} else if (getMetaUHC(e.getItemInHand()).equalsIgnoreCase("DiamondHead")) {
			e.setCancelled(true);
		} else if (getMetaUHC(e.getItemInHand()).equalsIgnoreCase("Head")) {
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}

	@EventHandler
	public void onOreBreak(final BlockBreakEvent e) {
		if (getMetaUHC(e.getPlayer().getItemInHand()).equalsIgnoreCase("Philo")) {
			if (e.getPlayer().getItemInHand().getDurability() == 1561) {
				e.getPlayer().getItemInHand().setDurability((short) 1559);

				Bukkit.getScheduler().runTaskLaterAsynchronously(UHC.getInstance(), new Runnable() {

					@Override
					public void run() {
						e.getPlayer().setItemInHand(new ItemStack(Material.AIR));

					}
				}, 0);
			}
		}
	}

	public String getMetaUHC(ItemStack i) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return "null";
		}

		return item.getTag().getString("UHC");
	}

	@SuppressWarnings("unused")
	private ItemStack[] shapeToMatrix(String[] shape, Map<Character, ItemStack> map, ShapedRecipe shaped) {
		/*
		 * List<Character> chars = new ArrayList<>(map.keySet()); for (int i = 1; i <
		 * chars.size(); i++) { if (!map.get(chars.get(i)).hasItemMeta() &
		 * !map.get(chars.get(i)).getItemMeta().hasDisplayName()) map.put(chars.get(i),
		 * ItemWithDisplayName(map.get(chars.get(i)), shaped, i)); }
		 */
		ItemStack[] matrix = new ItemStack[9];
		int slot = 0;

		for (int r = 0; r < shape.length; r++) {
			for (char col : shape[r].toCharArray()) {
				matrix[slot] = map.get(col);
				if (matrix[slot] == null)
					matrix[slot] = new ItemStack(Material.AIR, 0);
				slot++;
			}
			slot = ((r + 1) * 3);
		}
		return matrix;
	}

	public static ItemStack getSmeltedItemStack(final ItemStack itemStack) {
		switch (itemStack.getType()) {
		case PORK: {
			return new ItemStack(Material.GRILLED_PORK, itemStack.getAmount());
		}
		case RAW_BEEF: {
			return new ItemStack(Material.COOKED_BEEF, itemStack.getAmount());
		}
		case RAW_CHICKEN: {
			return new ItemStack(Material.COOKED_CHICKEN, itemStack.getAmount());
		}
		case RAW_FISH: {
			if (itemStack.getDurability() > 1) {
				return null;
			}
			return new ItemStack(Material.COOKED_FISH, itemStack.getAmount(), itemStack.getDurability());
		}
		case POTATO_ITEM: {
			return new ItemStack(Material.BAKED_POTATO, itemStack.getAmount());
		}
		case MUTTON: {
			return new ItemStack(Material.COOKED_MUTTON, itemStack.getAmount());
		}
		case RABBIT: {
			return new ItemStack(Material.COOKED_RABBIT, itemStack.getAmount());
		}
		case IRON_ORE: {
			return new ItemStack(Material.IRON_INGOT, itemStack.getAmount());
		}
		case GOLD_ORE: {
			return new ItemStack(Material.GOLD_INGOT, itemStack.getAmount());
		}
		case SAND: {
			return new ItemStack(Material.GLASS, itemStack.getAmount());
		}
		case COBBLESTONE: {
			return new ItemStack(Material.STONE, itemStack.getAmount());
		}
		case CLAY_BALL: {
			return new ItemStack(Material.CLAY_BRICK, itemStack.getAmount());
		}
		case NETHERRACK: {
			return new ItemStack(Material.NETHER_BRICK_ITEM, itemStack.getAmount());
		}
		case CLAY: {
			return new ItemStack(Material.HARD_CLAY, itemStack.getAmount());
		}
		case SMOOTH_BRICK: {
			return new ItemStack(Material.SMOOTH_BRICK, itemStack.getAmount(), (short) 2);
		}
		case DIAMOND_ORE: {
			return new ItemStack(Material.DIAMOND, itemStack.getAmount());
		}
		case LAPIS_ORE: {
			return new ItemStack(Material.INK_SACK, itemStack.getAmount(), (short) 4);
		}
		case REDSTONE_ORE: {
			return new ItemStack(Material.REDSTONE, itemStack.getAmount());
		}
		case COAL_ORE: {
			return new ItemStack(Material.COAL, itemStack.getAmount());
		}
		case EMERALD_ORE: {
			return new ItemStack(Material.EMERALD, itemStack.getAmount());
		}
		case QUARTZ_ORE: {
			return new ItemStack(Material.QUARTZ, itemStack.getAmount());
		}
		case GOLD_SWORD:
		case GOLD_PICKAXE:
		case GOLD_AXE:
		case GOLD_SPADE:
		case GOLD_HOE:
		case GOLD_HELMET:
		case GOLD_CHESTPLATE:
		case GOLD_LEGGINGS:
		case GOLD_BOOTS:
		case GOLD_BARDING: {
			return new ItemStack(Material.GOLD_NUGGET, itemStack.getAmount());
		}
		case CACTUS: {
			return new ItemStack(Material.INK_SACK, itemStack.getAmount(), (short) 2);
		}
		case LOG: {
			return new ItemStack(Material.COAL, itemStack.getAmount(), (short) 1);
		}
		case LOG_2: {
			return new ItemStack(Material.COAL, itemStack.getAmount(), (short) 1);
		}
		case SPONGE: {
			if (itemStack.getDurability() < 1) {
				return null;
			}
			return new ItemStack(Material.SPONGE, itemStack.getAmount(), (short) 0);
		}
		default: {
			return null;
		}
		}
	}
}
