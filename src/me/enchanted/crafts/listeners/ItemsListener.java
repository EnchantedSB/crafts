package me.enchanted.crafts.listeners;

import static org.bukkit.Material.DIAMOND_AXE;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.bukkit.entity.Entity;

import me.enchanted.crafts.Crafts;
import me.enchanted.crafts.Items;
import me.enchanted.crafts.UHC;
import me.enchanted.crafts.utils.HomingTask;

public class ItemsListener implements Listener {

	private UHC plugin = UHC.getInstance();
	
	public static HashMap <UUID, Integer> playerkills = new HashMap<>();

	@EventHandler
	public void onAttackPerun(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();
			UUID uuid = attacker.getUniqueId();

			if (attacker.getItemInHand().getType().equals(DIAMOND_AXE)) {
				if (getMetaUHC(attacker.getItemInHand()).equalsIgnoreCase("AxeOfPerun")) {
					if (!plugin.cooldownperun.containsKey(uuid)) {
						attacked.getWorld().strikeLightningEffect(attacked.getLocation());
						attacked.setHealth(attacked.getHealth() - 3);
						plugin.cooldownperun.put(uuid, plugin.peruncooldowntime);
					}
				}
			}
		}
	}

	@EventHandler
	public void expertevent(PlayerInteractEvent e) {

		ArrayList<ItemStack> enchanteditems = new ArrayList<>();

		Player p = e.getPlayer();
		Action action = e.getAction();
		ItemStack helditem = e.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {

			if (helditem != null && helditem.getType() == Material.NETHER_STAR
					&& helditem.getItemMeta().equals(Items.expertSeal.getItemMeta())) {
				

				if(p.getItemInHand().getAmount() == 1) {
					p.setItemInHand(new ItemStack(Material.AIR));
				} else {
					p.getItemInHand().setAmount(p.getItemInHand().getAmount()-1);
				}

				for (ItemStack item : p.getInventory().getContents()) {
					if (item != null) {
						if (item.getEnchantments().size() > 0) {
							 Map<Enchantment, Integer> enchantments = new HashMap<>();
							 
							 enchantments.putAll(item.getEnchantments());
							
							 for(Enchantment en: enchantments.keySet()) {   
								 
								 int oldValue = enchantments.get(en);   
						         enchantments.replace(en, oldValue+1);                            
						         item.removeEnchantment(en);
						    }                 
						     item.addUnsafeEnchantments(enchantments);
						} 
					}
				}

				for (ItemStack item : p.getInventory().getArmorContents()) {
					if (item != null) {
						if (item.getEnchantments().size() > 0) {
							 Map<Enchantment, Integer> enchantments = new HashMap<>();
							 
							 enchantments.putAll(item.getEnchantments());
							
							 for(Enchantment en: enchantments.keySet()) {   
								 
								 int oldValue = enchantments.get(en);   
						         enchantments.replace(en, oldValue+1);                            
						         item.removeEnchantment(en);
						    }                 
						     item.addUnsafeEnchantments(enchantments);
						} 
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onDamageEvent(EntityDamageByEntityEvent e) {


		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();
			ItemStack helm = attacker.getInventory().getHelmet();

			if (helm != null && helm.getType() == Material.DIAMOND_HELMET) {
				if (attacker.getInventory().getHelmet().getItemMeta().equals(Items.exodus.getItemMeta())) {
					attacker.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 80, 0));
				}
			}
		}
	}
	
	@EventHandler
	public void eventArrowFired(EntityShootBowEvent e) {

		ArrayList<Integer> chancelist = new ArrayList<Integer>();
		chancelist.add(1);
		chancelist.add(2);
		chancelist.add(3);
		chancelist.add(4);

		Random random = new Random();
		int choice = random.nextInt(chancelist.size());
		int finalchoice = chancelist.get(choice);

		LivingEntity player = e.getEntity();

		if (finalchoice == 4) {
			if (((e.getEntity() instanceof Player)) && ((e.getProjectile() instanceof Arrow))) {
				if (((HumanEntity) player).getItemInHand().getItemMeta().equals(Items.artemis.getItemMeta())) {

					double minAngle = 6.283185307179586D;
					Entity minEntity = null;
					for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
						if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity))
								&& (!entity.isDead())) {
							Vector toTarget = entity.getLocation().toVector().clone()
									.subtract(player.getLocation().toVector());
							double angle = e.getProjectile().getVelocity().angle(toTarget);
							if (angle < minAngle) {
								minAngle = angle;
								minEntity = entity;
							}
						}
					}
					if (minEntity != null) {
						new HomingTask((Arrow) e.getProjectile(), (LivingEntity) minEntity, (Plugin) this);
					}
				}
			}
		}
	}

	public String getMetaUHC(ItemStack i) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return "null";
		}

		return item.getTag().getString("UHC");
	}
	
	public void deathevent(PlayerDeathEvent e) {
		
		Player killed = (Player) e.getEntity();
		Player killer = (Player) killed.getKiller();
		
		if(killed.getKiller() instanceof Player) {
		
			if(playerkills.containsKey(killer.getUniqueId())) {
				playerkills.replace(killer.getUniqueId(), playerkills.get(killer.getUniqueId()) + 1);
				
				if(killer.getInventory().contains(Items.bloodLust)) {
					
					for(ItemStack s: killer.getInventory().getContents()) {
						
						if(s.equals(Items.bloodLust)) {
							
							switch(playerkills.get(killer.getUniqueId())) {
							
							case 1:
								
								s.addEnchantment(Enchantment.DAMAGE_ALL, 2);
								break;
								
							case 3:
								
								s.addEnchantment(Enchantment.DAMAGE_ALL, 3);
								break;
								
							case 6:
								
								s.addEnchantment(Enchantment.DAMAGE_ALL, 4);
								break;
								
							case 10:
								s.addEnchantment(Enchantment.DAMAGE_ALL, 5);
								break;
								
							}
							
						}
					}
					
				}
				
			} else {
				playerkills.put(killer.getUniqueId(), 1);
			}
			

		}
		
	}
}
