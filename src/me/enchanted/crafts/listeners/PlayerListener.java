package me.enchanted.crafts.listeners;

import net.minecraft.server.v1_8_R3.AttributeInstance;
import net.minecraft.server.v1_8_R3.AttributeModifier;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.enchanted.crafts.Items;

public class PlayerListener implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		is = setMeta(is, "Head");
		SkullMeta sm = (SkullMeta) is.getItemMeta();
		
		sm.setOwner(e.getEntity().getName());
		sm.setDisplayName("�e" + e.getEntity().getName() + "'s Skull");
		
		is.setItemMeta(sm);
		
		e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), is);
	}
	
	@EventHandler
	public void onMob(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if(p.getItemInHand().equals(Items.dareDevil)) {
				e.setCancelled(true);
				if(p.getItemInHand().getAmount() == 1) {
					p.setItemInHand(new ItemStack(Material.AIR));
				} else {
					p.getItemInHand().setAmount(p.getItemInHand().getAmount()-1);
				}
                Horse h = (Horse) e.getPlayer().getWorld().spawnEntity(e.getClickedBlock().getLocation().add(0,2,0), EntityType.HORSE);
                h.setTamed(true);
                h.setVariant(Variant.SKELETON_HORSE);
                h.setAdult();
                h.setMaxHealth(50.0D);
                h.setHealth(50.0D);
                
                AttributeInstance attributes = ((EntityInsentient)((CraftEntity)h).getHandle()).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
                attributes.setValue(0.45);
			}
		}
	}
	
	public ItemStack setMeta(ItemStack i, String value) {

		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);

		NBTTagCompound compound = new NBTTagCompound();

		compound.setString("UHC", value);

		Itemstack.setTag(compound);

		i = CraftItemStack.asBukkitCopy(Itemstack);

		return i;
	}

	public String getMetaUHC(ItemStack i) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return "null";
		}

		return item.getTag().getString("UHC");
	}

}
